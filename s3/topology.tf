resource "aws_s3_bucket" "my_s3_bucket" {
  bucket = "mys3bucket3455626"

  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}