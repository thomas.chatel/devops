data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "local_file" "my_lf" {
    content  = aws_instance.web.public_ip
    filename = "./ansible/inv.ini"
}

resource "aws_key_pair" "my_key_pair" {
  key_name   = "ansible_ssh_key"
  public_key = var.ssh_public_key
}

resource "aws_instance" "web" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.my_subnet_a.id
  key_name               = aws_key_pair.my_key_pair.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.my_sg.id]
  lifecycle {
    ignore_changes = ["associate_public_ip_address"]
  }
}

resource "aws_subnet" "my_subnet_a" {
  cidr_block        = "10.0.58.0/24"
  availability_zone = var.availability_zone
  vpc_id            = var.vpc_id
}

resource "aws_security_group" "my_sg" {
  name_prefix = var.user_name
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "sgr_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_sg.id
}

resource "aws_security_group_rule" "sgr_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_sg.id
}

resource "aws_security_group_rule" "sgr_node" {
  type              = "ingress"
  from_port         = 3000
  to_port           = 3000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_sg.id
}

resource "aws_security_group_rule" "sgr_egress_http" {
  type              = "egress"
  from_port         = 0
  to_port           = 60000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_sg.id
}

resource "aws_route_table_association" "my_rta" {
  subnet_id      = aws_subnet.my_subnet_a.id
  route_table_id = var.route_table_id
}

terraform {
  backend "s3" {
    bucket = "mys3bucket3455626"
    key = "terraform/terraform.tfstate"
    region = "eu-west-1"
    encrypt = true
  }
}

output "id" {
  value = aws_instance.web.public_ip
}