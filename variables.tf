variable "instance_type" {
  type        = string
  default     = "t2.micro"
}

variable "availability_zone" {
  type        = string
  default     = "eu-west-1a"
}

variable "region" {
  type        = string
  default     = "eu-west-1"
}

variable "vpc_id" {
    type = string
}

variable "route_table_id" {
    type = string
}

variable "user_name" {
    type = string
}

variable "s3_key" {
    type = string
    default = "terraform/terraform.tfstate"
}

variable "s3_bucket" {
    type = string
    default = "my_s3_bucket"
}

variable "dynamodb_table_name" {
    type = string
    default = "my_dynamodb_table"
}

variable "ssh_public_key" {
    type = string
}